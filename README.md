# ionic-component-sampler

Branch collection of different Ionic v4 techniques. This includes custom components developed that could be useful in other future projects, animation examples, different hotfix examples, etc. Descriptions are included in branch commits. 